# Flusso delle richieste

Per richieste intendiamo una chiamata che richiede una risorsa. La logica del flusso è molto simile sia per applicazioni
con architettura tradizionele che per le applicazioni web.

In generale lo schema è:

Form -> Request -> Task

La Form acquisisce i dati, li passa alla Request che a sua volta attiva il Task specifico a cui passa i dati. Nel caso
dell'applicazione Web, il task è rappresentato dal metodo associato tramite il meccanismo di Route. In altri contesti
viene utilizzato un Task Manager che nonostante il nome diverso persegue un compito equivalente.

Nella logica del flusso possiamo distinguere diversi livelli di controllo, distribuiti tra i vari coomponenti.
Ricordiamo sempre che la ridondanza dovuta alla ripetizione dei controlli tra i diversi livelli è dovuta alla necessità
di verificare i dati da parte del controller e rilevare gli errori formali il prima possibile.

La Form può avere una certa dose d'intelligenza e intercettare input non validi rifutandosi di inoltrarli alla Request.
La Request può effettuare ulteriori controlli, aggiungendo verifiche sull'esistenza o valore di specifici dati sul DB.
Infine il Task effettua le operazioni richieste.

## Mai fidarsi dei client

La verifica finale dei dati spetta sempre al Task (metodo del controller) che non deve mai fidarsi del client