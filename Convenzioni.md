# Convenzioni

## Naming convention

Relative alla creazione di componenti; utilizzando una convenzione per la creazione dei nomi (variabili, classi, ecc.)
divente più semplice comprendere il codice di un progetto. L'utilizzo di convenzioni ci permette di automatizzare
diversi aspetti della stesura del codice: pendiamo ai metodi _magici_ di PHP e Laravel: ci forniscono le funzionalità
che altrimenti dovremmo implementare scrivendo classi o metodi specifici.

## Lo sviluppo non è una scuola di pensiero

Le metodologie di sviluppo e le scelte tecniche devono essere ben definite e seguite quindi è importante che le
procedure legate alla vita di un'applicazione (sviluppo e manutenzione) siano seguite con cura. Se pensi che seguire
o no degli standard tecnici e formali sia solo una questione di scuola di pensiero, abbandona pure la lettura e medita
se per te non sia meglio filosofeggiare mentre zappi la terra.

## Pensare orientato agli oggetti

Ormai PHP è un linguaggio ad oggetti piuttosto evoluto ma spesso l'approccio alla programmazione continua ad avvalersi
della visione procedurale invece che ad oggetti.

Potremmo dire che la programmazione ad oggetti inverte il modo di vedere ed affrontare un problema: supponiamo di avere
un record di DB che rappresenta un ordine, nel modo procedurale avremo qualcosa tipo

```php
$data1 = [
    'campo1' => 'valore1',
    'campo2' => 'valore2',
    'campo3' => 'valore3',
    'campo4' => 'valore4',
]

function save1($data) {
    Sconnection->save('mytable',$data);
}

function print1($data) {
    // genera la stampa
}

function generaPrdf1($data)
{
   // genera il pdf
}

$data2 = [
    'campoa' => 'valorea',
    'campob' => 'valoreb',
    'campoc' => 'valorec',
]

function savea($data) {
    Sconnection->save('mytable',$dataa;
}

function printa($data) {
    //genera la stampa
}

function generaPrdfa($data2)
{
   // genera il pdf
}


```

se dobbiamo trattare anche dati di tipo diverso, possiamo duplicare il coduice e modificarlo ma avremo il problema dei
nomi di funzione che dovranno essere differenti inoltre ogni volta che dobbiamo richiamare una di quelle funzioni,
dovremo ricordarci quale sia quella corretta per il tipo di dato passato.

Utilizzando la programmazione ad oggetti, definiremo qualcosa di simile:

```php
class MioDato1 {
    public function __construct($data)
    {...}

    public function print()
    {...}
    
    public function generaPdf()
    {...}
}

class MioDato2 {
    public function __construct($data)
    {...}

    public function print()
    {...}
    
    public function generaPdf()
    {...}
}

...

$dato1 = new MioDato1([...]);
$dato1->print();
$dato1->generaPdf();

$dato2 = new MioDato2([...]);
$dato2->print();
$dato2->generaPdf();


```

MioDato è diventato la rappresentazione delle informazioni contenute nell'array con l'aggiunta della capacità di
_stamparsi_ e generare un pdf. All'utilizzatore interessa solo più come creare l'oggetto e come richiamarne i
metodi, lasciando il dettaglio dell'implementazione a chi sviluppa la classe relativa.

