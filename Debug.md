# Problematiche dello sviluppo

Lo sviluppo del software comporta solo una parte dei costi totali che aumenteranno con la manutenzione. Un sistema non
raggiungerà mai una forma stabile a causa di funzionalità da aggiungere o modificare ed agli inevitabili errori sia di
stesura del codice che di progettazione di qualche compoonente.

La manutenzione del codice richiede per prima cosa la comprensione della logica del flusso qiundi l'individuazione dei
punti da modificare ed infine la modifica o aggiunta del codice.

## Importanza delle convenzioni

I sistemi informatici sono fondati su parecchie convenzioni, anche se non ce ne rendiamo conto, questo documento può
essere letto come testo formattato grazie a dei simboli convenzionali infilati nel testo.
Gli stessi simboli, come tutti gli altri caratteri, fanno riferimento ad delle convenzioni per cui un certo valore
binario (o numerico) viene associato ad una particolare posizione nell'alfabeto di una certa lingua.

Un esempio dei problemi causati dall'adozione di convenzioni differenti è quello che per anni ha perseguitato gli
integratori di sistemi: la comunicazione fra sistemi con codifiche ACII e altri con EBCDIC e la necessità di creare 
convertitori.

Negli anni sono nati degli standard come CSV, XML e JSON che facilitano la vita agli sviluppatori definendo delle 
convenzioni comuni per il passaggio delle informazioni.

### Importanza della pulizia

Il codice va scritto mantenendo la maggiore pulizia possibile, pensando sempre che potrebbe capitare tra le mani di
qualcuno  che deve modificarlo, non conosce il progetto e non ha la possibilità di consultare l'autore.

### Spaghetti programming

Sebbene si possano ottenere gli stessi risultati (a livello di esecuzione e funzionalità) con software scritto male o
scritto bene, con il primo ci ritroveremo nei guai nel momento in cui sia da correggere e, solitamente,tale necessità
richiede interventi veloci.
Nella mia esperienza ho visto programmi non banali scritti in un unico blocco, funzionanti ma ingestibili dal punto di
vista della manutenzione; flussi pilotati con condizioni incomprensibili, if annidati in sottolivelli esagerati e
insalate di goto (per fortuna in PHP non si usa) 

### Rileggi il codice

Sebbene non abbia mai capito la mentalità di chi ha scritto il pezzo di codice seguente, ritengo che una rilettura del
codice avrebbe potuto evitarli una figura da stupido:

codice originale

```
...
operazione che può creare o no il file
...
delete(file)
...
```

ovviamente la procedura si piantrava quando l'operazione precedente alla delete non generava il file quindi la soluzione
è stata:

```
...
operazione che può creare o no il file
...
se il file non esiste crea(file)
...
delete(file)
...
```

Rileggendo il codice con un minimo di attenzione probabilmente l'autore avrebbe potuto notare l'assurdità della
soluzione. Sappiate quindi che se vedo del codice scritto con simili criteri, gli iinsulti sono garantiti.

