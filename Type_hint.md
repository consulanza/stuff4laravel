# Type Hint: la definizione di tipo di variabili e parametri

## Controllo stretto sui tipi 

Il controllo dei tipi in PHP può essere rinforzato utilizzando la direttiva ```declare(strict_types=1)```. La direttiva
deve essere inserita nei file interessati come **prima istruzione** e forza un controllo stretto sui tipi per gli
assegnamenti ed i parametri.

Sebbene non sia obbligatorio, l'utilizzo di questa direttiva comporta diversi vantaggi impedendo l'utilizzo errato di
variabili di tipo differente da quanto ci aspettiamo.

Il controllo stretto viene applicato solamente ai file in cui viene inserita la direttiva.

## Dischiarazione di tipo

Affinchè il controllo stretto sui tipi sia efficiente, dobbiamo dichiarare il tipo per variabili, parametri e risultati
delle funzioni. Per quanto a prima vista sembri solo aggiungere del lavoro, offre diversi vantaggi: 

- il codice rimane più comprensibile, i classici commenti phpDoc per indicare il tipo delle variabili, dei parametri o
  dei valori restituiti non sono necessari in quanto le dichiarazioni, in quel senso, sono autoesplicative
- i commenti phpDoc (ovviamente senza descrizioni) possono essere generati automaticamente
- gli ambienti di sviluppo possono controllare il codice mentre lo scriviamo e rilevare incompatibilità prima che il
  codice stesso venga eseguito

Prendiamo ad esempio una porzione di codice:

```php
$bug;

function fix($bug) 
```

Senza una documentazione specifica, possiamo solo presumere che quella funzione provveda a correggere un errore ma
senza un'ulteriore documentazione fornita dal programmatore dovremo andare a vedere il codice (*) per capire come
dobbiamo passarle i parametri e se, e nal caso cosa, restituisca un risultato.

```php
BugModel $bug;

function fix(BugModel $bug): bool
```

Anche in questo caso la funzionalità non ci viene descritta ma sappiamo che la funzione si applica ad un'istanza
della classe BugModel e che ci restituisce un valore booleano, presumibilmente indicante se l'operazione abbia avuto
esito positivo o no.

Da parte di chi sviluppa la funzione si ottiene il vantaggio di essere certi che la stessa riceva i parametri nel
formato corretto e da parte dell'utilizzatore si un'indicazione più chiara sui parametri che la funzione accetta
e sul risultato restituito.


```php
BugModel $bug;

function fix(BugModel $bug): bool
{
    ...
}

function resolveBug(): bool
{
    ...
    
    $bug = [
        'error' => 'someerror',
        'file' => $filename 
    ];
    
    return fix($bug);
}

```

nella funzione resolveBug il programmatore ha scordato di istanziare un oggetto BugModel e cerca di passarere alla
funzione fix() un array. Senza il controllo sui tipi questo errore può essere rilevato solo in esecuzione o con  una 
attenta simulazione mentre utilizzando il controllo verrà rilevato in fase di compilazione o, se l'IDE utilizzato
integra PHP, addirittura in fase di stesura del codice.

 


