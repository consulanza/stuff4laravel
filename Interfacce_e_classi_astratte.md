# Contracts: utilizzo di interfacce e classi astratte

## Interfacce

Le interfacce costituiscono uno strato di astrazione che definisce in modo formale i metodi che devono essere esposti
dalle classi che le implementano. Imponendo allo sviluppatore i metodi da implementare e le relative **firme**
(_method signature_) ovvero il numero e tipo di argomenti, si potrà scrivere del codice utilizzatore che richiama i metodi
degli oggetti senza la necessità di preoccuparsi quale implementazione specifica stia utilizzando.

Supponiamo di dover utilizzare il servizio API di un provider; avremo bisogno di una classe che sappia
colloquiare con il provider e trasformare le nostra richiesta nelle chiamate API necessarie.

Se sviluppiamo per conto nostro più classi per poter accedere ai servizi di provider diversi, probabilmente saremo
portati a creare, per le differenti classi, metodi con nomi e firme uguali ma classi scritte da terzi hanno una buona
probabilità di essere differenti.

L'interfaccia ha quindi il compito di definire l'aspetto _esterno_ dei metodi delle classi che vogliano implementare
un certo tipo di funzionalità e la clausola di implementazione impone che siano rispettati i requisiti (firme 
dell'interfaccia) del servizio.

### Case study: _servizio di invio di SMS_

Il motivo principale per utilizzare le interfacce in questo caso è la possibilità di utilizzare differenti provider e
superare l'ostacolo dovuto alle differenze tra le API tra di essi. La logica di invio di un messaggio non è molto
complessa e nel nostro caso richiede:

- mittente
- destinatario
- testo del messaggio

la nostra interfaccia dovrà quindi richiedere i metodi

- ```from(string $sender)``` per impostare il mittente
- ```to(string $target)``` per impostare il destinatario
- ```message(string $message)``` per impostare il testo del messaggio
- ```send()``` per inviare il messaggio tramite le API del fornitore

Dove viene utilizzato il codice che necessita del servizio, non dovremo preoccuparci delle operazioni di comunicazione
con il provider se utilizzeremo classi specifiche che presentano la stessa interfaccia. Questo tipo di classi
rappresenta un _**driver**_ per il servizio.

In questo modo sarà facile da parti terze implementare delle classi compatibili che utilizzano i servizi di provider
differenti.

```php
interface SmsDriverInterface
{
    public function from(string $sender): self;
    public function to(string $target): self;
    public function message(string $message): self;
    public function send(): self;
}
```

un utilizzatore dovrà quindi poter richiamare ```message('testo')->to('+390123456')->from('+390234567')->send()```
per inviare un messaggio.

### Aggiunta di un broker

Una volta definita l'interfaccia per i driver, possiamo creare una sorta di cappello che funga da **mediatore**
(_broker_) per semplificare ulteriormente l'utilizzo del servizio.

Nel nostro caso il mediatore è un gateway, ovvero un dispositivo che instrada le richieste verso differenti protocolli
(API di differenti provider), prendendosi il compito di nascondere i dettagli dell'implementazione.

Il nostro gateway offrirà i metodi:

```
- ```from(string $sender)``` per impostare il mittente
- ```to(string $target)``` per impostare il destinatario
- ```message(string $message)``` per impostare il testo del messaggio
- ```send()``` per inviare il messaggio tramite le API del fornitore
- ```via(string $driver)``` per indicare il driver utilizzato
```

Possiamo vedere come i metodi pubbli ci corrisponsano a quelli dell'interfaccia del driver più il metodo 
```via(string $driver)``` che serve ad indicare quale driver/fornitore andrà utilizzato. Il gateway provvederà in modo
trasparente a istanziare la classe interessata ed utilizzarla per colloquiare con il provider.

 ## Classi atratte

Le classi astratte contribuiscono ulteriormente a rafforzate i formalismi dei metodi e permettono la creazione di
metodi generalizzati, eraditati dai discendenti che provvederanno a personalizzare alcuni aspetti secondo le proprie
esigenze. Nel nostro esempio utilizziamo una classe astratta per i driver che implementa l'interfaccia e fornisce i metodi generici
di base.

```php
abstract class AbstractSmsDriver implements SmsDriverInterface
{
    protected string $sender = '';
    protected string $target = '';
    protected string $body = '';

    public function from(string $sender): self
    {
        $this->sender = $sender;
    }
    
    public function to(string $target): self
    {
        $this->target = $target;
    }
    
    public function message(string $message): self
    {
        $this->message = $message;
    }

    public function send(): self
    {
        $this->getApiCredentials();
        $this->preparePayload();
        $this->execApiSend();
    }
    
    abstract protected function getApiCredentials();   

    abstract protected function preparePayload();

    abstract protected function execApiSend();

}
```

Siccome abbiamo intenzione di implementare driver per diversi fornitori, utilizzeremo la struttura comune fornita
dalla classe _**AbstractSmsDriver**_ nella quale implementiamo i metodi più semplici

- ```from()```
- ```to()``` 
- ```message()```

il metodo generalizzato

- ```send()```

ed i metodi astratti generici

- ```getApiCredentials()```   

- ```preparePayload()```

- ```execApiSend()```

Vediamo che il metodo ```send()``` richiama i metodi generici che dovremo implementare nelle classi specifiche per i
diversi fornitori e che prevedono:

- getApiCredentials()<br>
  deve reperire parametri e credenziali API dalla configurazione
- preparePayload()<br>
  deve preparare il payload ovvero i dati (normalmente un array) che andranno passati nella chiamata API
- execApiSend()<br>
  deve effettuare la chiamata API

Le classi driver finali dovranno quindi implementare solamenti i metodi protetti astratti in cui occuparsi dei dettagli
come la connessione, l'autenticazione e l'invio della richiesta al fornitore.

