# Interfacce e binding delle classi

Vediamo come le [interfacce](Interfacce.md) possano essere legate ad uno specifico modello ed i vantaggi che tale
tecnica offre.

## Binding dell'interfaccia alla classe

Laravel ci permette di legare (_binding_) una specifica classe ad un modello, in tale modo qualsiasi codice che conosca
l'interfaccia sarà in grado di interagire con la classe effettiva senza neanche la necessità di sapere quale sia.
Quando necessario, il codice si limiterà ad istanziare l'interfaccia, Laravel provvederà a recuperare la classe
effettiva e rendere disponibile l'oggetto; di conseguenza un componente potrà sovrascrivere il _binding_ per forzare
l'utilizzo di uno specifico modello, eventualmente con estensioni o modifiche interne rispetto all'originale.


## Case study: Sistema di autenticazione

Normalmente in Laravel viene utilizzato un sistema di autenticazione basto sul modello ```App\Models\User``` e le 
personalizzazioni applicative vengono implementate in quel modello.

Se definiamo un'interfacce generica per il modello di autenticazione, saremo avvantaggiati nell'implementare diverse
soluzioni, per esigenze differenti, basandoci su un modello comune.

### AuthUserInterface e AuthUser

Supponiamo di definire un'interfaccia che descriva i metodi disponibili per la classe User

```php
interface AuthUserInterface
{
    ... 
}
```

ed un modello derivato dall'originale con la clausola di implementazione

```php
class AuthUser extends User implements AuthUserInterface
{
    ...
}
```

Se tutti metodi che trattano oggetti di quel tipo fanno riferimento a _**AuthUserInterface**_ invece che direttamente
a _**AuthUser**_, potremo gestire tipologie di utenti con una base unificata ed estensioni specifiche.

### Binding in Laravel

Per effettuare il binding, laravel fornisce un metodo di facile uso: nel ServiceProvider dell'applicazione o del modulo
andrà aggiunta l'istruzione: ```$this->app->bind(AuthUserInterface::class, AuthUser::class);```

