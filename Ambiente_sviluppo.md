# Ambiente di sviluppo per Laravel

## Requisiti generali

Un ambiente di sviluppo web (php/mysql) ha alcuni requisiti particolari rispetto ai normali pc da ufficio:

- poter utilizzare differenti versioni di PHP
- poter utilizzare differenti versioni di MySQL o MariaDB
- poter accedere al DB e manipolarlo tramite client grafici
- disporre di un webserver
- poter eseguire comandi di utilità offerti dal framework

## Versioni di PHP

Se sul sistema è possibile installare differenti versioni di PHP, potremo utilizzare un semplice script shell per
impostare quella da utilizzare, ad esempio su un sistema GNU/Linux possiamo utilizzare lo script seguente per
selezionare la versione tra 7.4, 8.0, 8.1, 8.2.

```shell
#!/bin/bash
#
# Set current CLI PHP Version
# Copyright (C) 2021 Amedeo Lanza - ConsuLanza Informatica
# License: GPLv3
#
MENUHEIGHT=20
MENUWIDTH=0
SELECTED_VERSION=""
#
PHPVER=`php -v`
#
exec 3>&1;
SELECTION=$(dialog \
--keep-tite \
--backtitle "${PHPVER}" \
--title "Selezione versione" \
--cancel-label "Annulla" \
--no-lines \
--menu "Seleziona La versione di PHP da utilizzare da linea comandi" ${MENUHEIGHT} ${MENUWIDTH} 50 \
0	"7.4" \
1	"8.0" \
2	"8.1" \
3	"8.2" 2>&1 1>&3);

return_value=$?
exec 3>&-
#
dialog --clear
case $SELECTION in
    0)
    SELECTED_VERSION='7.4'
    echo "7.4"
    echo ""
    ;;
    1)
    SELECTED_VERSION='8.0'
    echo "8.0"
    echo ""
    ;;
    2)
    SELECTED_VERSION='8.1'
    echo "8.1"
    echo ""
    ;;
    3)
    SELECTED_VERSION='8.2'
    echo "8.2"
    echo ""
    ;;
esac
if [ "${SELECTED_VERSION}" = "" ]
then
    exit
fi
sudo update-alternatives --set php /usr/bin/php${SELECTED_VERSION}
sudo update-alternatives --set phar /usr/bin/phar${SELECTED_VERSION}
sudo update-alternatives --set phar.phar /usr/bin/phar.phar${SELECTED_VERSION}
```

un risultato simile può essere ottenuto utilizzando una serie di container Docker, avviando quello necessario per
la versione di PHP interessata. Tutte le istanze dovrebbero utilizzare le stesse condivisioni e rimarrebbe la necessità
di accedere al container per eseguire i comandi delle utility o comandi console.

Per gli ambienti Windows potete seguire i link di seguito per iniziare a trovare informazioni su come impostarlo per
rendere le operazioni il più possibile trasparenti.

- https://blog.devsense.com/2022/develop-php-in-docker
- https://medium.com/cook-php/running-different-versions-of-php-under-windows-b16db9d864ee

### Alcuni svantaggi di Docker

- Per l'esecuzione di docker-compose in ambiente *nix è necessario aggiungere il proprio utente al gruppo docker con il
comando ```usermod -a -G docker <utente>```; 

## Proprietà e permessi del file system

L'accesso al file system deve essere garantito sia allo sviluppatore che al processo che esegue il webserver.
Normalmente il progetto viene mantenuto in un filesystem di proprietà dello sviluppatore non accessibile da processi di
utenze estranee, cosa che obbliga ad (almeno) una delle soluzioni

- il filesystem viene sprotetto per permettere a chiunque l'accesso (decisamente deprecato)
- l'utente che esegue il webserver viene aggiunto nel gruppo dell'utente proprietario del filesystem ed i permessi calibrati per un accesso sicuro (deprecato)
- il processo che esegue php utilizza UID e GID del proprietario del file system (ottimale)

per il punto 3 possiamo:

- eseguire l'applicazione tramite php artisan serve
- utilizzare apache con php-fpm per eseguire il processo con GID e UID del proprietario del file system

la prima soluzione è estremamente comoda e non richiede di uscire dall'ide mentre nel secondo caso offrirebbe un 
ambiente più elastico ma più complesso da gestire (permette di avere in linea contemporaneamente più _siti_ con versioni
diverse di PHP ma richiede la configurazione di Apache e php-fpm)


## Database

Per il database l'utilizzo di Docker può risultare molto versatile: per ogni versione di database interessata possiamo 
utilizzare un container specifico; eventualmente possono essere attivi contemporaneamente se messi in ascolto su porte
differenti. Anche se infilato in un container, il DB rimane accessibile dall'esterno tramite TCP quindi non è necessario
installare applicativi come phpMyAdmin quando possono essere tranquillamente usati strumenti come mysql client,
mysqldump o [DBeaver](https://dbeaver.io/), semplicemente indicando l'host nella connessione.

### Socket del container non disponibile 

Il container non espone il socket quindi le connessioni al Db devono essere effettuate o dentro al container o via TCP.
Utilizzando solamente l'accesso via rete, non è necessario che nel container siano aggiunti altri strumenti come PHP,
Apache (o NGINX) e phpMyqdmin

### Dump 

Per effettuare il dump del database basta avere a disposizione mysqldump, ricordando di indicare ```-h localhost```.

### Restore e client mysql

Il client mysql può essere utilizzato ricordando di indicare ```-h localhost```.
As edempio :

```shell
mysql -h localhost -P 3306 --protocol=tcp -uroot -p&lt;password&gt; &lt;database&gt;  &lt; &lt;file&gt;.sql 
```

### Utenza

Per comodità sul server mysql locale può essere creato un utente standard che abbia accesso a tutti i database,
configurato nel file .env.default

## Web server

Il mantenimento di una configurazione complessa di Apache e php-fpm richiede uno sforzo che può essere evitato se si
utilizzano gli strumenti a disposizione con Laravel:

```php artisan serve```

per avviare il webserver ambedded, con la gestione corretta dei file .htaccess e redirect (simula il comportamento di
Apache). Il server rimane in ascolto sulla porta 8000, disponibile per verificare le pagine web (con interfaccia
tradizionale) e collaudare le chiamate API con client come Postman o AdvancedRestClient.

```npm run dev```

per avviare l'interfaccia Vue 

