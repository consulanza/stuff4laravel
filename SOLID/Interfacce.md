# Le interfacce

Le interfacce costituiscono la descrizione astratta, concisa e formale dei metodi che una classe rende disponibile ai
propri utilizzatori quindi non contiene dichiarazioni di variabili e/o costanti nè l'implementazione dei metodi ma
esclusivamente le **intestazioni** o **firme** (_**signature**_) dei metodi pubblici. Le classi che implementano
un'interfaccia dovranno definire i metodi dichiarati in essa e tutto il resto necessario per il loro funzionamento 
(costanti, variabili, metodi privati ecc.).

Il valore aggiunto dall'utilizzo delle interfacce è costituito da:

- possibilità di implementare classi compatibili senza doverle necessariamente derivare tutte dalla stessa
- possibilità di implementare classi compatibili con più interfacce (simile a ereditarietà multipla)
- possibilità di effettuare il [_**binding**_](Interfacce_e_binding_classi.md) di specifiche classi

## Estensione delle interfacce

Supponiamo che uno sviluppatore ci fornisca un package contenete le dichiarazioni:

```php
interface CanPlayInterface
{
    public function play();
    public function pause();
}
```

l'interfaccia descrive un semplice player multimediale con la capacità di avviare e fermare la riproduzione.

```php
class Player implements CanPlayInterface
{
    ..
    
    public function play()
    {
        ...
    }
    public function pause()
    {
        ...
    }
}
```

la classe Player implementa l'interfaccia e possiamo utilizzarla per riprodurre i nostri brani. Ad un certo punto
avremo la necessità di aggiungere funzionalità al player, come ad esempio **_shuffle_** e **_repeat_** ma non abbiamo
la possibilità di modificare e/o ampliare il codice della classe Player. Per ottenere le funzionalità possiamo estendere
l'interfaccia aggiungendo i metodi per le funzionalità interessate e derivare una nuova classe dal Player:

```php
interface CanSortListInterface extends CanPlayInterface
{
    public function shuffle();
    public function repeat();
}
```

```php
class MyPlayer extends Player implements CanSortListInterface
{
    ...
    
    public function shuffle()
    {
        ...
    }
    public function repeat()
    {
        ...
    }
}
```

## Trait

Un buon metodo per implementare classi con la medesima interfaccia è costituito dall'utilizzo dei _**Trait**_ che ci
permettono di includere porzioni di codice nelle stesse; nel trait possiamo implementare il codice necessario per
rendere una classe compatibile con le caratteristiche (ovvero i metodi pubblici) richieste dall'interfaccia.

Riprendendo l'eesempio precedente, possiamo spostare nei trait il codice relativo alle funzionalità dell'interfaccia:

```php
trait CanPlay
{
    ..
    
    public function play()
    {
        ...
    }
    public function pause()
    {
        ...
    }
}
```

ed includerlo nelle classi che devono implementare quell'interfaccia

```php
class Player implements CanPlayInterface
{
    use CanPlay;
}
```

riportiamo in un trait anche il codice relativo all'interfaccia CanSortListInterface:

```php
trait CanSortList
{
    ..
    
    public function shuffle()
    {
        ...
    }
    public function repeat()
    {
        ...
    }
}
```

ed avremo due possibilità per ottenere la nostra classe finale:

```php
class MyPlayer extends Player implements CanSortListInterface
{
    use CanSortList;
}
```

che semplicemente estende la classe Player (via ereditarietà) ed aggiunge le funzionalità per l'ordinamento delle liste
oppure una nuova classe, formalmente non imparentata con Player ma compatibile grazie all'interfaccia.


```php
class MyPlayer implements CanSortListInterface
{
    use CanPlay, CanSortList;
}
```

## Versatilità migliorata

L'utilizzo delle interfacce rende più versatili i componenti dal momento che aggiunge un livello di astrazione
importante per la creazione di classi che sfruttino il polimorfismo delle tecniche OOP.

## Conclusioni

Le interfacce permettono la definizione formale dei metodi pubblici che una classe deve implementare
