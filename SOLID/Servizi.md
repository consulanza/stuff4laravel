# Servizi

I servizi sono uno dei capisaldi per avere un'architettuda solita e strumento per:

- isolare il codice per la logica del flusso dagli altri componenti
- facilitare il riutilizzo del codice evitandone la duplicazione
- delegare un compito specifico ad un componente dedicato

La separazione di compiti e responsabilità dei componenti si riflette anche sui componenti del gruppo di sviluppo
e la metodologia dello sviluppo: separando le diverse problematiche, il gruppo potrà lavorare dedicando una persona
per ogni compito specifico e gli sviluppatori potranno lavorare contemporaneamente senza pestarsi i piedi e creare
una marea di conflitti per i merge. Uno sviluppatore può dedicarsi alla creazione dei servizi mentre un altro 
provvede a mettere a punto le request ed il controller ed un terzo si occupa dell'interfaccia web. Questo permette
inoltre di distribuire lo sviluppo in modo più mirato alle diverse competenze dei membri del gruppo.



## Case Study: sistema per la gestione di documenti con tracciamento dello stato

Il nostro sistema prevede un meccanismo per la gestione di stati e relative transizioni applicati ai documenti.
In questo momento non ci interessa il meccanismo per la gestione degli stati ma l'implementazione dei servizi e delle
API per interfacciarlo.

In particolare, nel controller abbiamo i seguenti metodi:

```php

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(CreateRequest $request): JsonResponse
    {
        $validated = $request->validated();
        
        $pathRicevuta = '';
        if ($request->hasFile('ricevuta')) {
            $destDir = sprintf('documents/temp/');
            $pathRicevuta = $request->file('ricevuta')->store($destDir);
        }
        
        $documento = $this->documentoService->create($validated);
        return $this->success(
            new DocumentoResource($documento),
            __('created'),
            ResponseAlias::HTTP_CREATED
        );
    }

    /**
     * Update item
     * @param UpdateRequest $request
     * @param Documento $documento
     * @return JsonResponse
     * @throws \Exception
     */
    public function update(UpdateRequest $request, Documento $documento): JsonResponse
    {
        $validated = $request->validated();
        $newData = $validated;
        $documento = $this->documentoService->update($documento, $newData);
        return $this->success(new DocumentoResource($documento));
    }

    /**
     * Require item signature
     * @param RequireSignRequest $request
     * @param Documento $documento
     * @return JsonResponse
     * @throws \Exception
     */
    public function requireSignature(RequireSignRequest $request, Documento $documento): JsonResponse
    {
        $validated = $request->validated();
        $item = $this->documentoService->requireSignature($documento);
        return $this->success(['autocertificazione' => new DocumentooResource($item), 'mobile' => $documento->socio->cell]);
    }
```

Possiamo notare come i metodi siano compatti e non effettuino direttamente operazioni
oltre a ricevere i dati, validarli, passarli al metodo di un servizio esterno al
controller e restituire il risultato del metodo richiamato.

La classe che offre il servizio contiene i metodi:

```php
 /**
     * Crea il documento
     * @param array $data
     * @param string $pathRicevuta
     * @return Documento
     * @throws Exception
     */
    public function create(array $data, string $pathRicevuta = ''): Documento
    {
        DB::beginTransaction();
        try {
            // crea il record tramite factory, lo salva tramite repository e lo restituisce
            $documento = $this->autocertificazioneFactory->make($data);
            $documento->save();
            $this->addHistory($documento, Created::statusName(), __('documenti::notes.created'));

            if ($pathRicevuta != '' && file_exists($pathRicevuta)) {
                // salva e registra la ricevuta
                $this->addDRicevuta($documento, $pathRicevuta);
            }

            $this->addPdf($documento);

        } catch (Throwable $e) {
            DB::rollBack();
            $msg = __('documenti::error.creating_model', ['modelname' => 'Documento']);
            $prev = $e->getMessage();
            throw new Exception(sprintf('%s: %s', $msg, $prev));
        }
        DB::commit();
        return $documento;
    }


    /**
     * Aggiorna il documento
     * @param Documento $item
     * @param array $newData
     * @return Documento
     * @throws Exception
     */
    public function update(Documento $item, array $newData)
    {
        $newState = new Created(DocumentoState::class);
        $this->allowTransition($item->state, $newState);

        DB::beginTransaction();
        try {
            $item->state = $newState;
            $item->update($newData);
            $item->save();
            $this->addHistory($item, $newState->statusName(), __('documenti::notes.updated'));
        } catch (Throwable $e) {
            DB::rollBack();
            $msg = __('documenti::error.updating_model', ['modelname' => 'Documento']);
            $prev = $e->getMessage();
            throw new Exception(sprintf('%s: %s', $msg, $prev));
        }
        DB::commit();
        return $item;
    }

    /**
     * Riceve la richiesta di firma, crea il codice, salva il documento aggiornata col codice
     * e lo invia all'utente via SMS
     * @param Documento $item
     * @return Documento
     * @throws Exception
     */
    public function requireSignature(Documento $item)
    {
        $newState = new Created(DocumentoState::class);
        $this->allowTransition($item->state, $newState);

        try {
            $item->state = $newState;
            $item->secret = $this->makeSecret();
            $item->save();

            $this->addHistory($item, $newState->statusName(), __('documenti::notes.sign_requested'));
            $this->sendConfirmCode($item);
            return $item;
        } catch (Exception $e) {
            DB::rollBack();
            $msg = __('documenti::error.requiring_sign', ['modelname' => 'Documento']);
            $prev = $e->getMessage();
            throw new Exception(sprintf('%s: %s', $msg, $prev));
        }
    }

```

Possiamo notare come i metodi effettuino delle diverse operazioni che se inserite direttamente nel controller,
ne aumenterebbero notevolmente la complessità. Utilizzando una classe di servizio isoliamo quelle operazioni e
ne deleghiamo la responsabilità ad una classe dedicata che gestisce correttamente la logica del flusso.

Questa metodologia ci permette inoltre di riutilizzare facilmente la stessa classe di servizo in altre situazioni
come un evento, una procedura batch o qualsiasi altra occasione per cui un metodo vada utilizzato al di fuori del
controller. Ne consegue che rimane più semplice utilizzare e/o creare strumenti per i test come phpUnit o comandi
console che simulino una serie di interazioni con l'utente.

Nell'esempio che segue vediamo un comando console che simula il percorso di un documento, utile a verificare che
le operazioni effettuate dal servizio funzionino correttamente. In questo modo il codice del servizio può essere
facilmente debuggato senza la complessità aggiundiva del dover utilizzare strumenti come Postman integrando il debug
remoto dell'applicazione.

```php

class TestDocumentoCommand extends Command
{

    protected $signature = 'autocertifica:documento';

    protected $description = 'Command description';

    protected int $pnt_ana = 35;
    protected int $specialita_id = 3;
    protected int $modified_specialita_id = 4;

    public function __construct(
        private DocumentoService $documentoService
    )
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $this->testTransitions();

    }

    /**
     * @throws Exception
     */
    protected function testTransitions()
    {
        $documento = $this->createDocumento();
        $documento = $this->modifyDocumento($documento);
        $documento = $this->requireSignature($documento);
        $documento = $this->confirmSignature($documento);
        //$items = $this->listCertificates();
        $this->testStoredMedia($documento);

        try {
            $documento = $this->approveDocumento($documento);
        } catch (Exception $e) {
            $this->output->text('Errore cambio stato intercettato in approve');
        }
        try {
            $documento = $this->denyDocumento($documento);
        } catch (Exception $e) {
            $this->output->text('Errore cambio stato intercettato in deny');
        }

        try {
            $documento = $this->expireDocumento($documento);
        } catch (Exception $e) {
            $this->output->text('Errore cambio stato intercettato in expire');
        }
        // $this->testDownload($documento);

    }

    protected function createDocumento(): Documento
    {
        $fromDir = storage_path('temp/documenti');
        $fileName = 'ricevuta_bollettino.txt';
        $filePath = "/" . $fromDir . DIRECTORY_SEPARATOR . $fileName;
        if (!file_exists($fromDir)) {
           mkdir($fromDir, 0755, true);
        }
        file_put_contents($filePath,'ricevuta pagamento bollettino');

        $data = [
        ...
        ];

        $documento = $this->documentoService->create($data, $filePath);
        $this->output->text(var_export(new DocumentoResource($documento), true));
        return $documento;
    }

    protected function modifyDocumento(Documento $documento): Documento
    {
        $data = [
        ...
        ];
        $documento = $this->documentoService->update($documento, $data);
        $this->output->text(var_export(new DocumentoResource($documento), true));
        return $documento;
    }

    protected function requireSignature(Documento $documento): Documento
    {
        $documento = $this->documentoService->requireSignature($documento);
        $this->output->text(var_export(new DocumentoResource($documento), true));
        return $documento;
    }

    protected function confirmSignature(Documento $documento): Documento
    {
        $documento = $this->documentoService->requireSignature($documento);
        $this->output->text(var_export(new DocumentoResource($documento), true));
        return $documento;
    }

    /**
     * @return Collection
     */
    protected function listCertificates(): Collection
    {
        $text = '';
        $items = $this->documentoService->all();


        foreach ($items as $item) {

            $this->output->text(var_export($item->getAttributes(), true));
        }

        return $items;
    }

    protected function testStoredMedia(Documento $documento)
    {
        $documents = $documento->multimedias()->get();
        foreach ($documents as $document) {
            $title = $document->title;
            $file_path = $document->file_path;
            $description = $document->description;
            $url = Storage::url($file_path);
            $absolutePath = Storage::path($file_path);
            if (Storage::exists($file_path)) {
                $message = sprintf('Documento trovato: titolo: %s decrizione: %s path: %s Abs. path: %s url: %s', $title, $description, $file_path, $absolutePath, $url);
                $this->output->text($message);
            }
        }
    }

    protected function testDownload(Documento $documento)
    {
        $this->documentoService->downloadPdf($documento);
    }

    protected function approveDocumento($documento)
    {
        return $this->documentoService->approve($documento, 'Test transizioni');
    }
    protected function denyDocumento($documento)
    {
        return $this->documentoService->deny($documento, 'Test transizioni');
    }
    protected function expireDocumento($documento)
    {
        return $this->documentoService->expire($documento, 'Test transizioni');
    }

}

```

Se avessimo inserito il codice dei servizi direttamente nel controller, per ottenere un comando console come quello
dell'esempio, avremmo dovuto duplicarlo, con la conseguenza di dover in futuro riportare modifiche e correzioni a
entrambi. E l'esperienza mi dice che, oltre a raddoppiare il lavoro, la manutenzione di quel codice è destinata a
diventare un incubo richiedendo un inutilo spreco di energie per mantenerlo allineato.