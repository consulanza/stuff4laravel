# Scope

Gli **_Scope_** sono dei filtri di visibilità che permettono l'incapsulamento dei criteri di ricerca per Eloquent.
Il loro utilizzo uniforma e semplifica la gestione dei criteri utilizzati più frequentemente, facilitando l'isolamento
delle responsabilità dei componenti.

## Nome degli scope

Il nome degli scope è formato da scope segioto dal nome del filtro, ad esempio per un filtro sulla condizione attivo
avremo il metodo scopeActive() che potrà essere utilizzato applicando il modificatore active() alla query.

## Locali

Gli scope locali sono definiti nel modello e tipicamente definiscono filtri specifici per lo stesso.

Un semplice esempio consiste nella definizione di un paio di filtri per elencare solo gli utenti attivi o con un punteggio alto:

```php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    /**
    * Scope a query to only include popular users.
    *
    * @param \Illuminate\Database\Eloquent\Builder $query
    * @return \Illuminate\Database\Eloquent\Builder
    */
    public function scopePopular($query)
    {
        return $query->where('votes', '>', 100);
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }
}
```
per utilizzare i filtri potremo quindi richiamare:

```php
// elenca gli utenti più popolari
$popular_users = User::popular()->get();

// elenca gli utenti attivi
$active_users = User::active()->get();

// elenca gli utenti attivi più popolari 
$popular_active_users = User::active()->popular()->get();
```

I metodi di scope vanno definiti anteponendo scope al nome con cui vogliamo poi utilizzarli, quindi nel nostro esempio:
```scopePopular()``` e ```scopeActive()```.