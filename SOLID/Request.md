# Utilizzo delle FormRequest

Con la classe _**FormRequest**_ Laravel offre uno strumento per:

- isolare il codice per il controllo formale da quello del flusso
- facilitare il riutilizzo del codice evitandone la duplicazione
- delegare un compito specifico ad un componente dedicato

## Case Study: sistema per la gestione di documenti con tracciamento dello stato

Il nostro sistema prevede un meccanismo per la gestione di stati e relative transizioni applicati ai documenti.
In questo momento non ci interessa il meccanismo per la gestione degli stati ma l'implementazione dei servizi e delle
API per interfacciarlo.

In particolare, nel controller abbiamo i seguenti metodi:

```php
    /**
     * Change status to Verified and save admin's note in history
     * @param ChangeStatusRequest $request
     * @param Documento $documento
     * @return JsonResponse
     */
    public function approve(ChangeStatusRequest $request, Documento $documento): JsonResponse
    {
        $validated = $request->validate();
        $documento = $this->documentoService->approve($documento, $validated['note']);
        return $this->success(new DocumentoResource($documento));
    }

    /**
     * Change status to Denied and save admin's note in history
     * @param ChangeStatusRequest $request
     * @param Documento $documento
     * @return JsonResponse
     */
    public function deny(ChangeStatusRequest $request, Documento $documento): JsonResponse
    {
        $validated = $request->validate();
        $documento =  $this->documentoService->deny($documento, $validated['note']);
        return $this->success(new DocumentoResource($documento));
    }

    /**
     * @param ChangeStatusRequest $request
     * @param Documento $documento
     * @return JsonResponse
     */
    public function expire(ChangeStatusRequest $request, Documento $documento): JsonResponse
    {
        $validated = $request->validate();
        $documento =  $this->documentoService->expire($documento, $validated['note']);
        return $this->success(new DocumentoResource($documento));
    }

    /**
     * @param ChangeStatusRequest $request
     * @param Documento $documento
     * @return JsonResponse
     */
    public function reject(ChangeStatusRequest $request, Documento $documento): JsonResponse
    {
        $validated = $request->validate();
        $documento =  $this->documentoService->expire($documento, $validated['note']);
        return $this->success(new DocumentoResource($documento));
    }
```

tutti questi metodi utilizzano la stessa request di classe ChangeStatusRequest

```php
class ChangeStatusRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'id' => [
                'required',
                'integer',
                Rule::exists('documents', 'id'),
            ],
            'note' => [
                'required|min 10',
            ]
        ];
    }
}
```

come possiamo vedere, la request è estremamente semplice e potrebbe sembrare inutile, visto che definisce solamente le
regole per due campi; regole che potrebbero essere infilate nel metodo del controller. Ma cosa succede quando la
validazione è la stessa per più metodi del controller ?

- duplichiamo lo stesso codice nei diversi metodi
- scriviamo una funzione che ci restituisca le regole e la utilizziamo in tutti i metodi interessati
- scriviamo una request e la facciamo utilizzare a tutti i metodi interessati

appare evidente come la prima soluzione sia controproducente:

- il codice viene duplicato
- se vanno modificate le regole bisogna intervenire in ogni metodo che le usa

il secondo metodo ci risolve i due problemi appena elencati ma ha ancora dei lati controproducenti:

- la visibilità del metodo che effettua la verifica dovrebbe essere privata o al massimo protetta, questo va bene quando
tutti i metodi che la utilizzano sono nello stesso controller o controller derivati.
- viene lasciata al controller la responsabilità di un compito che tecnicamente non è il suo

Tra le metodologie per mantenere un progetto gestibile, Laravel offre la possibilità di creare controller single-action
quando mantenerne uno unico diventerebbe eccessivamente complesso o per poter aggiungere delle funzionalità senza dover
modificare i controller preesistenti. Utilizzando le FormRequest potremo quindi condividere i metodi di controllo non
solo con metodi diversi dello stesso controller ma anche con metodi di altri controller.

## Vantaggi utilizzando le request

Esaminando i metodi riportati sopra possiamo notare:

- La _**signature**_ del metodo ci indica subito quale insieme di regole (classe ChangeStatusRequest) verrà utilizzato,
- La validazione nel controller viene effettuata con la singola istruzione ````$validated = $request->validate();````,
se non siamo interessati alla validazione, non saremo distratti da una sbrodolata di codice _estraneo_.
- La responsabilità della validazione viene correttamente delegata alla request (principi SOLID)
- La request può essere utilizzata per la validazione in qualsiasi metodi di qualsiasi controller

Laravel offre gli strumenti necessari per sviluppare un'architettura pulita e funzionale ma anche tante scorciatoie
che per quanto molto efficaci per sviluppare velocemente, bypassano alcuni principi fondamentali per la creazione di
sistemi stabili e manutenibili.

## Conclusioni

L'utilizzo delle Request risponde alla necessità di **isolare le funzionalità** e **delegarne la responsabilità**,
**riutilizzare il codice**


